# Static Site Generator

The python file uses Jinja2 (or Jinja 3, but it's still called Jinja2) templates to render html files and copy over static files when its ready to be built. Haven't decided if I will use JS with this, but I think it would be a fun project to try and see what I can do without it. This will be a personal site where I just do whatever, put things up on display, anything really.
